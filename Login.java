import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class Login {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String username;
    private static String password;
    private static String base_url;
    private static String notValidPass;
    private static String notValidUser1;
    private static String notValidUser2;

    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        base_url = prop.getProperty("base_url");
        notValidPass = prop.getProperty("notValidPass");
        notValidUser1 = prop.getProperty("notValidUser1");
        notValidUser2 = prop.getProperty("notValidUser2");

    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword(){
        return password;
    }

    public static String getBase_url(){
        return base_url;
    }
    public static String getNotValidPass(){
        return notValidPass;
    }
    public static String getNotValidUser1(){
        return base_url;
    }
    public static String getNotValidUser2(){
        return base_url;
    }
    //Правильные пароль и логин
    @Test
    void valid(){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", getPassword())
                .when()
                .post(getBase_url() + "gateway/login")
                .then()
                .statusCode(200);

        }

        //Не правильный пароль
    @Test
    void notValidPass(){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", getNotValidPass())
                .when()
                .post(getBase_url() + "gateway/login")
                .then()
                .statusCode(401);
    }

    //Не правильный логин
    @Test
    void notValidName(){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getNotValidUser2())
                .formParam("password", getPassword())
                .when()
                .post(getBase_url() + "gateway/login")
                .then()
                .statusCode(401);
    }
//getNotValidUser2()   getPassword()

    //Логин менее 3 символов
    @Test
    void minName(){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getNotValidUser1())
                .formParam("password", getPassword())
                .when()
                .post(getBase_url() + "gateway/login")
                .then()
                .statusCode(401);
    }

    }


