import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class NotMyPosts extends AbstractTest{

    @Test
    void page1OrderASC(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", 1)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .statusCode(200);


    }

    @Test
    void page10OrderDESC() {
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .queryParam("page", 10)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .header("X-Powered-By", "PHP/8.1.10")
                .contentType("application/json");
    }

    void emptyPage(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("page", 10000)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)

    }

}
