import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Token extends AbstractTest{
    @Test
    void reqToken (){
        Object response = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", getPassword())
                .when()
                .post(getBase_url() + "gateway/login")
                .then().extract()
                .jsonPath()
                .get("token")
                .toString();

        System.out.println("Token: "+ response);
        }
}

